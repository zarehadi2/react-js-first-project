

const baseURL = 'http://localhost:3050/cars';

const getCollecttionURL = () => baseURL;

const getElementURL = carId =>
    `${getCollecttionURL()}/${encodeURI(carId)}`;

export const getAllCars = async () => {
    const res = await fetch(getCollecttionURL());
    const cars = await res.json();
    // return cars;

    return new Promise(resolve =>
        setTimeout(()=>
            resolve(cars), 1000));
}

export const createCar = async (car) => {
    const res = await fetch(getCollecttionURL(), {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(car),
    });
    const savedCar = await res.json();
    return savedCar;
}

export const replaceCar = async (car) => {
    const res = await fetch(getElementURL(car.id), {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(car),
    });
    const savedCar = await res.json();
    return savedCar;
}

export const deleteCar = async (carId) => {
    await fetch(getElementURL(carId), {
        method: 'DELETE',
    });
}