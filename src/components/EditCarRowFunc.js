import React from 'react';
import PropTypes from 'prop-types';

import { carPropType } from '../propTypes/cars';
import { useForm } from '../hooks/useForm'
import { useDefaultInputFocus } from '../hooks/useDefaultInputFocus'


export const EditCarRow = ({ 
    car,
    onSaveCar,
    onCancelCar: cancelCar
}) => {

    const [ carForm, change ] = useForm({ ...car });

    const defaultInputRef = useDefaultInputFocus();

    const saveCar = () => {
        onSaveCar({
            ...carForm,
            id: car.id,
        })
    }

    return <tr>
        <td>{car.id}</td>
        <td>
            <input id="edit-make-input" type="text" name="make" ref={defaultInputRef}
                value={carForm.make} onChange={change} />
        </td>
        <td>
            <input id="edit-model-input" type="text" name="model" 
                value={carForm.model} onChange={change} />
        </td>
        <td>
            <input id="edit-year-input" type="number" name="year" 
                value={carForm.year} onChange={change} />
        </td>
        <td>
            <input id="edit-color-input" type="text" name="color" 
                value={carForm.color} onChange={change} />
        </td>
        <td>
            <input id="edit-price-input" type="number" name="price" 
                value={carForm.price} onChange={change} />
        </td>
        <td>
            <button type="button" onClick={saveCar}>Save</button>
        </td>
        <td>
            <button type="button" onClick={cancelCar}>Cancel</button>
        </td>
    </tr>;
};

EditCarRow.propTypes = {
    car: carPropType.isRequired,
    onSaveCar: PropTypes.func.isRequired,
    onCancelCar: PropTypes.func.isRequired,
}