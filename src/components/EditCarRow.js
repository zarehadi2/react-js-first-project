import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';

import { carPropType } from '../propTypes/cars';


export class EditCarRow extends Component { 

    constructor(props){
        super(props);
        this.state = {
            ...props.car
        };

        this.defaultInputRef = createRef();

        this.change = this.change.bind(this);
        this.saveCar = this.saveCar.bind(this);
    }

    componentDidMount(){
        if(this.defaultInputRef.current){
            this.defaultInputRef.current.focus();
        }
    }

    change({target: { name, type, value }}) {
        this.setState({
            [ name ] : type === "number" 
            ? Number(value) 
            : value
        });
    }

    saveCar(){
        this.props.onSaveCar({
            ...this.state,
            id: this.props.car.id,
        })
    }

    render(){
        return <tr>
        <td>{this.props.car.id}</td>
        <td>
            <input id="edit-make-input" type="text" name="make"
                ref={this.defaultInputRef}
                value={this.state.make} onChange={this.change} />
        </td>
        <td>
            <input id="edit-model-input" type="text" name="model" 
                value={this.state.model} onChange={this.change} />
        </td>
        <td>
            <input id="edit-year-input" type="number" name="year" 
                value={this.state.year} onChange={this.change} />
        </td>
        <td>
            <input id="edit-color-input" type="text" name="color" 
                value={this.state.color} onChange={this.change} />
        </td>
        <td>
            <input id="edit-price-input" type="number" name="price" 
                value={this.state.price} onChange={this.change} />
        </td>
        <td>
            <button type="button" onClick={this.saveCar}>Save</button>
        </td>
        <td>
            <button type="button" onClick={this.props.onCancelCar}>Cancel</button>
        </td>
    </tr>;
    }
}
   
EditCarRow.propTypes = {
    car: carPropType.isRequired,
    onSaveCar: PropTypes.func.isRequired,
    onCancelCar: PropTypes.func.isRequired,
}